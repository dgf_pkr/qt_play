#include "findcrap.h"
#include "ui_findcrap.h"
#include <QIODevice>
#include <QFile>
#include <QTextStream>
#include <QTextCursor>

FindCrap::FindCrap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FindCrap)
{
    ui->setupUi(this);

    //opens the text file
    getTextFile();
}

FindCrap::~FindCrap()
{
    delete ui;
}

void FindCrap::on_goButton_clicked()
{
    //lineEdit is the line edit element
    QString word = ui->lineEdit->text();
    ui->textEditMain->find(word, QTextDocument::FindWholeWords);


}

void FindCrap::getTextFile()
{
    QFile myFile("N:\\Programming\\qt_play"
                 "\\BuckyRoberts\\FindCrap\\hello.txt");
    myFile.open(QIODevice::ReadOnly);
    QTextStream textStream (&myFile);
    QString line = textStream.readAll();
    myFile.close();

    //ui is the entire ui (the outermost box).textEditMain is the
    //the textEdit area (and I renaned it to textEditMain in the
    //designer
    ui->textEditMain->setPlainText(line);

    //Show user where word is
    QTextCursor textCursor = ui->textEditMain->textCursor();
    textCursor.movePosition(QTextCursor::Start,
                            QTextCursor::MoveAnchor, 1);


}
