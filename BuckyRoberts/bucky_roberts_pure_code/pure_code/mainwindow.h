#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSpinBox>
#include <QHBoxLayout>

class MainWindow
{
public:
    MainWindow(QHBoxLayout* layout, int* sizeArray,
               QString windowTitle);
    ~MainWindow();
private:
    QWidget *w = 0;
};

class SpinBox : public QSpinBox
{
public:
    SpinBox();
    ~SpinBox();
};





#endif // MAINWINDOW_H
