#include "mainwindow.h"
#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QSlider>


int main(int argc, char *argv[])
{
    QApplication prog(argc, argv);

    QSpinBox *spinner = new QSpinBox;
    QSlider *slider = new QSlider(Qt::Horizontal);
    spinner->setRange(1,50);
    slider->setRange(1,50);
    QObject::connect(spinner, SIGNAL(valueChanged(int)),
            slider, SLOT(setValue(int)));
    QObject::connect(slider, SIGNAL(valueChanged(int)),
            spinner, SLOT(setValue(int)));
    spinner->setValue(10);

    QPushButton *button = new QPushButton("Quit the program!");
    QObject::connect(button, SIGNAL(clicked()), &prog, SLOT(quit()));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(slider);
    layout->addWidget(spinner);
    layout->addWidget(button);

    int size_array[] ={600,100};
    MainWindow mainWindow(layout,size_array,
                          "How many chicken winges");


    return prog.exec();
}

//TODO : redesign (put out of main) and try to emulate what Qt does
//automatically - check at the end.

