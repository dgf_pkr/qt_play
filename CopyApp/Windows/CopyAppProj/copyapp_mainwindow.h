#ifndef COPYAPP_MAINWINDOW_H
#define COPYAPP_MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class CopyApp_MainWindow;
}

class CopyApp_MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CopyApp_MainWindow(QWidget *parent = 0);
    ~CopyApp_MainWindow();

   int addAction(QAction *);

private:
    Ui::CopyApp_MainWindow *ui;
};

#endif // COPYAPP_MAINWINDOW_H
